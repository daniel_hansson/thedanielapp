//
//  ViewController.m
//  TheDanielApp
//
//  Created by Daniel Hansson on 2016-01-28.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *stuffBtn;
@property (weak, nonatomic) IBOutlet UIButton *infoBtn;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)DoubleZize:(UIButton *)sender {
    self.view.backgroundColor = [UIColor colorWithRed:drand48()
                                                green:drand48()
                                                 blue:drand48()
                                                alpha:1];
    self.stuffBtn.tintColor =  [UIColor colorWithRed:drand48()
                                               green:drand48()
                                                blue:drand48()
                                               alpha:1];
    self.infoBtn.tintColor = [UIColor colorWithRed:drand48()
                                             green:drand48()
                                              blue:drand48()
                                             alpha:1];
    self.photoBtn.tintColor = [UIColor colorWithRed:drand48()
                                              green:drand48()
                                               blue:drand48()
                                              alpha:1];
    self.label.tintColor = [UIColor colorWithRed:drand48()
                                              green:drand48()
                                               blue:drand48()
                                              alpha:1];
}
- (IBAction)FontSize:(UISlider *)sender {
    double fontSize = self.slider.value;
    
    [self.label setFont: [UIFont fontWithName:@"Arial" size:fontSize]];
 
}


@end
